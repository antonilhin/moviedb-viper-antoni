//
//  Review.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 06/02/24.
//

import Foundation

struct Reviews: Codable {
    let id: Int
    let page: Int
    let results: [Review]
}

struct Review: Codable {
    let author: String
    let authorDetails: AuthorDetail
    let content: String
    let createdAt: String
    let id: String
    let url: String
}

struct AuthorDetail: Codable {
    let name: String?
    let username: String
    let avatarPath: String?
    let rating: Double?
}
