//
//  Video.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 06/02/24.
//

import Foundation

struct Videos: Codable {
    let id: Int
    let results: [Video]
}

struct Video: Codable {
    let name: String
    let key: String
    let site: String
    let size: Int
    let type: String
    let official: Bool
    let publishedAt: String
    let id: String
}
