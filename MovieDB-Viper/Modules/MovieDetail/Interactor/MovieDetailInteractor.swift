//
//  MovieDetailInteractor.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 08/02/24.
//

import Foundation

class MovieDetailInteractor: MovieDetailPresenterToInteractorProtocol {
    // MARK: - Properties
    weak var presenter: MovieDetailInteractorToPresenterProtocol?
    var videos: Videos?

    // MARK: - Functions
    func fetchMovieVideo(with movieID: Int) {
        APIClient.default.fetchRelatedVideos(with: movieID) { result in
            switch result {
            case .success(let data):
                self.videos = data
                self.presenter?.videosFetched()
            case .failure(_):
                self.presenter?.videosFetchedFailed()
            }
        }
    }
}
