//
//  ReviewsInteractor.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 08/02/24.
//

import Foundation

class ReviewsInteractor: ReviewsPresenterToInteractorProtocol {
    // MARK: - Properties
    weak var presenter: ReviewsInteractorToPresenterProtocol?
    var reviews: [Review] = []
    var isLoading = false
    var page = 1

    // MARK: - Functions
    func fetchReviews(with movieID: Int) {
        guard !isLoading else { return }
        isLoading = true

        APIClient.default.fetchMovieReview(with: movieID, page: page) { result in
            switch result {
            case .success(let data):
                self.reviews.append(contentsOf: data.results)
                self.isLoading = false
                self.page += 1
                self.presenter?.reviewsFetched()
            case .failure(_):
                self.isLoading = false
                self.presenter?.reviewsFetchedFailed()
            }
        }
    }
}
