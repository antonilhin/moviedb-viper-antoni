//
//  MovieDetailPresenter.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 08/02/24.
//

import Foundation

class MovieDetailPresenter: MovieDetailViewToPresenterProtocol {
    var view: MovieDetailPresenterToViewProtocol?
    var interactor: MovieDetailPresenterToInteractorProtocol?
    var router: MovieDetailPresenterToRouterProtocol?
    
    func viewDidLoad(with genreID: Int) {
        interactor?.fetchMovieVideo(with: genreID)
    }
    
    func getYouTubeTrailer() -> Video? {
        if let youtubeTrailer = interactor?.videos?.results.first(where: {$0.type == "Trailer"}) {
            return youtubeTrailer
        }
        return nil
    }
}

// MARK: - Interactor to Presenter
extension MovieDetailPresenter: MovieDetailInteractorToPresenterProtocol {
    func videosFetched() {
        view?.showMovieDetail()
    }

    func videosFetchedFailed() {
        view?.showError()
    }
}
