//
//  ReviewsPresenter.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 08/02/24.
//

import Foundation

class ReviewsPresenter: ReviewsViewToPresenterProtocol {
    var view: ReviewsPresenterToViewProtocol?
    var interactor: ReviewsPresenterToInteractorProtocol?
    var router: ReviewsPresenterToRouterProtocol?
    
    func viewDidLoad(with movieID: Int) {
        interactor?.fetchReviews(with: movieID)
    }

    func fetchMoreReviews(with movieID: Int) {
        interactor?.fetchReviews(with: movieID)
    }

    func isLoadingMore() -> Bool? {
        interactor?.isLoading
    }

    func getReviewsCount() -> Int? {
        interactor?.reviews.count
    }
    
    func getReviewAt(index: Int) -> Review? {
        return interactor?.reviews[index]
    }
}

// MARK: - Interactor to Presenter
extension ReviewsPresenter: ReviewsInteractorToPresenterProtocol {
    func reviewsFetched() {
        view?.showReviews()
    }
    
    func reviewsFetchedFailed() {
        view?.showError()
    }
    

}
