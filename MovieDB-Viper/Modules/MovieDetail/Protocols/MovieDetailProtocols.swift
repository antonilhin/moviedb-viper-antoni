//
//  MovieDetailProtocols.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 08/02/24.
//

import Foundation

// MARK: - View to Presenter
protocol MovieDetailViewToPresenterProtocol: AnyObject {
    var view: MovieDetailPresenterToViewProtocol? { get set }
    var interactor: MovieDetailPresenterToInteractorProtocol? { get set }
    var router: MovieDetailPresenterToRouterProtocol? { get set }

    func viewDidLoad(with genreID: Int)
    func getYouTubeTrailer() -> Video?
}

// MARK: - Presenter to View
protocol MovieDetailPresenterToViewProtocol: AnyObject {
    func showMovieDetail()
    func showError()
}

// MARK: - Presenter to Interactor
protocol MovieDetailPresenterToInteractorProtocol: AnyObject {
    var presenter: MovieDetailInteractorToPresenterProtocol? { get set }
    var videos: Videos? { get }

    func fetchMovieVideo(with genreID: Int)
}

// MARK: - Presenter to Router
protocol MovieDetailPresenterToRouterProtocol: AnyObject {
    static func createReviewModule() -> ReviewsViewController
}

// MARK: - Interactor to Presenter
protocol MovieDetailInteractorToPresenterProtocol: AnyObject {
    func videosFetched()
    func videosFetchedFailed()
}
