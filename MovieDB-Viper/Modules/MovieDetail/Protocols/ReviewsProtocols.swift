//
//  ReviewsProtocols.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 08/02/24.
//

import Foundation

// MARK: - View to Presenter
protocol ReviewsViewToPresenterProtocol: AnyObject {
    var view: ReviewsPresenterToViewProtocol? { get set }
    var interactor: ReviewsPresenterToInteractorProtocol? { get set }
    var router: ReviewsPresenterToRouterProtocol? { get set }

    func viewDidLoad(with movieID: Int)
    func fetchMoreReviews(with movieID: Int)
    func isLoadingMore() -> Bool?
    func getReviewsCount() -> Int?
    func getReviewAt(index: Int) -> Review?
}

// MARK: - Presenter to View
protocol ReviewsPresenterToViewProtocol: AnyObject {
    func showReviews()
    func showError()
}

// MARK: - Presenter to Interactor
protocol ReviewsPresenterToInteractorProtocol: AnyObject {
    var presenter: ReviewsInteractorToPresenterProtocol? { get set }
    var reviews: [Review] { get }
    var isLoading: Bool { get set }
    var page: Int { get set }

    func fetchReviews(with movieID: Int)
}

// MARK: - Presenter to Router
protocol ReviewsPresenterToRouterProtocol: AnyObject {
    static func createModule() -> ReviewsViewController
}

// MARK: - Interactor to Presenter
protocol ReviewsInteractorToPresenterProtocol: AnyObject {
    func reviewsFetched()
    func reviewsFetchedFailed()
}
