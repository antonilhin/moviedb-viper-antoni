//
//  MovieDetailRouter.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 08/02/24.
//

import Foundation

class MovieDetailRouter: MovieDetailPresenterToRouterProtocol {
    static func createReviewModule() -> ReviewsViewController {
        let view = ReviewsViewController()
        let presenter = ReviewsPresenter()
        let interactor = ReviewsInteractor()
        let router = ReviewsRouter()

        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter

        return view
    }
}
