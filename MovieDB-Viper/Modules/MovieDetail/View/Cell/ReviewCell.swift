//
//  ReviewCell.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 08/02/24.
//

import UIKit

class ReviewCell: UITableViewCell {
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var reviewLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configureCell(with model: Review) {
        let rating = String(model.authorDetails.rating?.roundToDecimal(1) ?? 0)
        ratingLabel.text = "\(rating) •"
        nameLabel.text = "@\(model.authorDetails.username)"
        reviewLabel.text = model.content
    }
}
