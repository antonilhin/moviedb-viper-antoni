//
//  MovieDetailViewController.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 08/02/24.
//

import SDWebImage
import UIKit

class MovieDetailViewController: UIViewController {
    // MARK: - IBOutlets
    @IBOutlet weak var backdropImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!

    // MARK: - Properties
    var presenter: MovieDetailViewToPresenterProtocol?
    var movie: Movie?

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad(with: movie?.id ?? 0)
    }

    func configureMovie(with model: Movie) {
        let imageUrl = URL(string: Constants.imageBaseUrl + (model.backdropPath ?? ""))

        // Assign components value
        backdropImageView.sd_setImage(with: imageUrl,
                                      placeholderImage: UIImage(named: "film-reel"))
        titleLabel.text = model.title
        overviewLabel.text = model.overview
    }

    // MARK: - IBActions
    @IBAction func watchTrailerAction(_ sender: UIButton) {
        // Open YouTube App or YouTube from default browser
        if let youTubeTrailer = presenter?.getYouTubeTrailer() {
            if let youtubeUrl = URL(string: Constants.youTubeWatchBaseUrl + youTubeTrailer.key) {
                UIApplication.shared.open(youtubeUrl, options: [:], completionHandler: nil)
            }
        }
    }
    
    @IBAction func openReviewAction(_ sender: UIButton) {
        let reviewVC = MovieDetailRouter.createReviewModule()
        reviewVC.movie = movie
        present(reviewVC, animated: true)
    }
}

// MARK: - Presnter to View
extension MovieDetailViewController: MovieDetailPresenterToViewProtocol {
    func showMovieDetail() {
        if let movie {
            DispatchQueue.main.async {
                self.configureMovie(with: movie)
            }
        }
    }

    func showError() {
        DispatchQueue.main.async {
            AlertView.showErrorFetching(view: self,
                                        title: Constants.generalErrorMessage,
                                        message: Constants.errorFetchingMovieDetail)
        }
    }
}
