//
//  ReviewsViewController.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 08/02/24.
//

import UIKit

class ReviewsViewController: UITableViewController {
    // MARK: - Properties
    private let reviewCellID = "ReviewCell"
    var presenter: ReviewsViewToPresenterProtocol?
    var movie: Movie?

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        presenter?.viewDidLoad(with: movie?.id ?? 0)
    }

    func configureView() {
        tableView.register(UINib(nibName: reviewCellID,
                                 bundle: nil),
                           forCellReuseIdentifier: reviewCellID)
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        if presenter?.getReviewsCount() == 0 {
            tableView.configureEmptyData("No reviews available")
        } else {
            tableView.restore()
        }
        return presenter?.getReviewsCount() ?? 0
    }

    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: reviewCellID,
                                                       for: indexPath) as? ReviewCell else {
            return UITableViewCell()
        }
        if let review = presenter?.getReviewAt(index: indexPath.row) {
            cell.configureCell(with: review)
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, 
                            didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, 
                            heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    override func tableView(_ tableView: UITableView,
                            estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    override func tableView(_ tableView: UITableView,
                   heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }

    override func tableView(_ tableView: UITableView,
                   heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }

    override func tableView(_ tableView: UITableView,
                   titleForHeaderInSection section: Int) -> String? {
      return "Reviews"
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if offsetY > contentHeight - scrollView.frame.height {
            if let isLoading = presenter?.isLoadingMore() {
                if !isLoading {
                    presenter?.fetchMoreReviews(with: movie?.id ?? 0)
                }
            }
        }
    }
}

// MARK: - Presenter to View
extension ReviewsViewController: ReviewsPresenterToViewProtocol {
    func showReviews() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func showError() {
        DispatchQueue.main.async {
            AlertView.showErrorFetching(view: self,
                                        title: Constants.generalErrorMessage,
                                        message: Constants.errorFetchingReviewsMsg)
        }
    }
}
