//
//  GenreListInteractor.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 07/02/24.
//

import Foundation

class GenreListInteractor: GenreListPresenterToInteractorProtocol {
    // MARK: - Properties
    weak var presenter: GenreListInteractorToPresenterProtocol?
    var genres: Genres?

    // MARK: - Functions
    func fetchGenres() {
        APIClient.default.fetchGenres { result in
            switch result {
            case .success(let data):
                self.genres = data
                self.presenter?.genresFetched()
            case .failure(_):
                self.presenter?.genresFetchedFailed()
            }
        }
    }
}
