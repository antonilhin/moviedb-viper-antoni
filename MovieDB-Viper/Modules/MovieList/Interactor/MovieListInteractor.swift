//
//  MovieListInteractor.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 07/02/24.
//

import Foundation

class MovieListInteractor: MovieListPresenterToInteractorProtocol {
    // MARK: - Properties
    weak var presenter: MovieListInteractorToPresenterProtocol?
    var movies: [Movie] = []
    var isLoading = false
    var page = 1

    // MARK: - Functions
    func fetchMovies(with movieID: Int) {
        guard !isLoading else { return }
        isLoading = true

        APIClient.default.fetchMovieByGenre(with: movieID, page: page) { result in
            switch result {
            case .success(let data):
                self.movies.append(contentsOf: data.results)
                self.isLoading = false
                self.page += 1
                self.presenter?.moviesFetched()
            case .failure(_):
                self.isLoading = false
                self.presenter?.moviesFetchedFailed()
            }
        }
    }
}
