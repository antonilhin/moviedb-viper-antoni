//
//  MovieListPresenter.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 07/02/24.
//

import Foundation
import UIKit

class GenreListPresenter: GenreListViewToPresenterProtocol {
    var view: GenreListPresenterToViewProtocol?
    var interactor: GenreListPresenterToInteractorProtocol?
    var router: GenreListPresenterToRouterProtocol?

    func viewDidLoad() {
        interactor?.fetchGenres()
    }
    
    func getGenresCount() -> Int? {
        interactor?.genres?.genres.count
    }
    
    func getGenreAt(index: Int) -> Genre? {
        return interactor?.genres?.genres[index]
    }
}

// MARK: - Interactor to Presenter
extension GenreListPresenter: GenreListInteractorToPresenterProtocol {
    func genresFetched() {
        view?.showGenres()
    }
    
    func genresFetchedFailed() {
        view?.showError()
    }
}
