//
//  MovieListPresenter.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 07/02/24.
//

import Foundation
import UIKit

class MovieListPresenter: MovieListViewToPresenterProtocol {
    var view: MovieListPresenterToViewProtocol?
    var interactor: MovieListPresenterToInteractorProtocol?
    var router: MovieListPresenterToRouterProtocol?
    
    func viewDidLoad(with genreID: Int) {
        interactor?.fetchMovies(with: genreID)
    }

    func fetchMoreMovies(with genreID: Int) {
        interactor?.fetchMovies(with: genreID)
    }

    func isLoadingMore() -> Bool? {
        interactor?.isLoading
    }

    func getMovieCount() -> Int? {
        interactor?.movies.count
    }
    
    func getMovieAt(index: Int) -> Movie? {
        return interactor?.movies[index]
    }
}

// MARK: - Interactor to Presenter
extension MovieListPresenter: MovieListInteractorToPresenterProtocol {
    func moviesFetched() {
        view?.showMovies()
    }
    
    func moviesFetchedFailed() {
        view?.showError()
    }
}
