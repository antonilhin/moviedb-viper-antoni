//
//  GenreListProtocols.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 07/02/24.
//

import Foundation

// MARK: - View to Presenter
protocol GenreListViewToPresenterProtocol: AnyObject {
    var view: GenreListPresenterToViewProtocol? { get set }
    var interactor: GenreListPresenterToInteractorProtocol? { get set }
    var router: GenreListPresenterToRouterProtocol? { get set }

    func viewDidLoad()
    func getGenresCount() -> Int?
    func getGenreAt(index: Int) -> Genre?
}

// MARK: - Presenter to View
protocol GenreListPresenterToViewProtocol: AnyObject {
    func showGenres()
    func showError()
}

// MARK: - Presenter to Interactor
protocol GenreListPresenterToInteractorProtocol: AnyObject {
    var presenter: GenreListInteractorToPresenterProtocol? { get set }
    var genres: Genres? { get }

    func fetchGenres()
}

// MARK: - Presenter to Router
protocol GenreListPresenterToRouterProtocol: AnyObject {
    static func createModule() -> GenreViewController
    static func createMovieListModule() -> MovieListViewController
}

// MARK: - Interactor to Presenter
protocol GenreListInteractorToPresenterProtocol: AnyObject {
    func genresFetched()
    func genresFetchedFailed()
}
