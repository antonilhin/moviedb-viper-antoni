//
//  MovieListProtocols.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 07/02/24.
//

import Foundation

// MARK: - View to Presenter
protocol MovieListViewToPresenterProtocol: AnyObject {
    var view: MovieListPresenterToViewProtocol? { get set }
    var interactor: MovieListPresenterToInteractorProtocol? { get set }
    var router: MovieListPresenterToRouterProtocol? { get set }

    func viewDidLoad(with movieID: Int)
    func fetchMoreMovies(with genreID: Int)
    func isLoadingMore() -> Bool?
    func getMovieCount() -> Int?
    func getMovieAt(index: Int) -> Movie?
}

// MARK: - Presenter to View
protocol MovieListPresenterToViewProtocol: AnyObject {
    func showMovies()
    func showError()
}

// MARK: - Presenter to Interactor
protocol MovieListPresenterToInteractorProtocol: AnyObject {
    var presenter: MovieListInteractorToPresenterProtocol? { get set }
    var movies: [Movie] { get }
    var isLoading: Bool { get set }
    var page: Int { get set }

    func fetchMovies(with genreID: Int)
}

// MARK: - Presenter to Router
protocol MovieListPresenterToRouterProtocol: AnyObject {
    static func createModule() -> MovieListViewController
    static func createMovieDetailModule() -> MovieDetailViewController
}

// MARK: - Interactor to Presenter
protocol MovieListInteractorToPresenterProtocol: AnyObject {
    func moviesFetched()
    func moviesFetchedFailed()
}
