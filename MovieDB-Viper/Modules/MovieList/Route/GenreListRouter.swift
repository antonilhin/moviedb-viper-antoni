//
//  GenreListRouter.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 07/02/24.
//

import Foundation
import UIKit

class GenreListRouter: GenreListPresenterToRouterProtocol {
    static func createModule() -> GenreViewController {
        let view = GenreViewController()
        let presenter = GenreListPresenter()
        let interactor = GenreListInteractor()
        let router = GenreListRouter()

        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter

        return view
    }

    static func createMovieListModule() -> MovieListViewController {
        let view = MovieListViewController()
        let presenter = MovieListPresenter()
        let interactor = MovieListInteractor()
        let router = MovieListRouter()

        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter

        return view
    }
}
