//
//  MovieListRouter.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 07/02/24.
//

import Foundation

class MovieListRouter: MovieListPresenterToRouterProtocol {
    static func createModule() -> MovieListViewController {
        let view = MovieListViewController()

        return view
    }
    
    static func createMovieDetailModule() -> MovieDetailViewController {
        let view = MovieDetailViewController()
        let presenter = MovieDetailPresenter()
        let interactor = MovieDetailInteractor()
        let router = MovieDetailRouter()

        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return view
    }
}
