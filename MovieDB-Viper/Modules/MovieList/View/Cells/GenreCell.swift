//
//  GenreCell.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 07/02/24.
//

import UIKit

class GenreCell: UICollectionViewCell {
    @IBOutlet weak var genreLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configureCell(with model: Genre) {
        genreLabel.text = model.name
    }
}
