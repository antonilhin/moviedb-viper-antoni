//
//  MovieCell.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 07/02/24.
//

import SDWebImage
import UIKit

class MovieCell: UITableViewCell {
    @IBOutlet weak var posterImageView: UIImageView! {
        didSet {
            // Styles
            posterImageView.layer.masksToBounds = false
            posterImageView.layer.cornerRadius = 18
            posterImageView.layer.shadowOffset = CGSize(width: 2.0, height: 4.0)
            posterImageView.layer.shadowRadius = 5
            posterImageView.layer.shadowOpacity = 1
        }
    }
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configureCell(with model: Movie) {
        let imageUrl = URL(string: Constants.imageBaseUrl + model.posterPath)
        let rating = String(model.voteAverage.roundToDecimal(1))
        let releaseDateAndMonth = convertDateFormat(inputDate: model.releaseDate)

        DispatchQueue.main.async {
            // Assign components value
            self.posterImageView.sd_setImage(with: imageUrl,
                                             placeholderImage: UIImage(named: "film-reel"))
            self.ratingLabel.text = rating
            self.titleLabel.text = model.title
            self.yearLabel.text = releaseDateAndMonth
            self.descriptionLabel.text = model.overview
        }
    }
}

// MARK: - Private helpers
private extension MovieCell {
    func convertDateFormat(inputDate: String) -> String {
        let olDateFormatter = DateFormatter()
        olDateFormatter.dateFormat = "yyyy-MM-dd"

        guard let oldDate = olDateFormatter.date(from: inputDate) else { return "" }

        let convertDateFormatter = DateFormatter()
        convertDateFormatter.dateFormat = "dd MMM yyyy "

        return convertDateFormatter.string(from: oldDate)
    }
}
