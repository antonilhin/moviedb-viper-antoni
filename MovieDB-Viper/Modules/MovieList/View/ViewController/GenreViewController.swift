//
//  GenreViewController.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 07/02/24.
//

import UIKit



class GenreViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Properties
    private let genreCellID = "GenreCell"
    var presenter: GenreListViewToPresenterProtocol?

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        presenter?.viewDidLoad()
    }

    func configureView() {
        title = "Genres"
        collectionView.register(UINib(nibName: genreCellID,
                                 bundle: nil),
                                forCellWithReuseIdentifier: genreCellID)
    }
}

// MARK: - UICollectionView Data Source
extension GenreViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if presenter?.getGenresCount() == 0 {
            collectionView.configureEmptyData("No genres available")
        } else {
            collectionView.restore()
        }
        return presenter?.getGenresCount() ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, 
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: genreCellID,
                                                       for: indexPath) as? GenreCell else {
            return UICollectionViewCell()
        }
        if let genre = presenter?.getGenreAt(index: indexPath.item) {
            cell.configureCell(with: genre)
        }
        cell.addBorderAndShadow()
        return cell
    }

    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        guard let selectedGenre = presenter?.getGenreAt(index: indexPath.item) else { return }

        let movieVC = GenreListRouter.createMovieListModule()
        movieVC.genre = selectedGenre

        navigationController?.pushViewController(movieVC, animated: true)
    }
}

// MARK: - CollectionView Delegate
extension GenreViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let layout = collectionViewLayout as? UICollectionViewFlowLayout else {
            return CGSize()
        }
        let widthPerItem = (collectionView.frame.width - 30) / 2 - layout.minimumInteritemSpacing
        return CGSize(width: widthPerItem, height: 180)
    }
}

// MARK: - Presenter To View
extension GenreViewController: GenreListPresenterToViewProtocol {
    func showGenres() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func showError() {
        DispatchQueue.main.async {
            AlertView.showErrorFetching(view: self,
                                        title: Constants.generalErrorMessage,
                                        message: Constants.errorFetchingGenresMsg)
        }
    }
}
