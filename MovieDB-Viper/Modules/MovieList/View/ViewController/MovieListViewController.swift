//
//  MovieListViewController.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 07/02/24.
//

import UIKit

class MovieListViewController: UITableViewController {
     
    // MARK: - Properties
    private let movieCellID = "MovieCell"
    var presenter: MovieListViewToPresenterProtocol?
    var genre: Genre?

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        presenter?.viewDidLoad(with: genre?.id ?? 0)
    }

    func configureView() {
        title = genre?.name ?? ""
        tableView.register(UINib(nibName: movieCellID,
                                 bundle: nil),
                           forCellReuseIdentifier: movieCellID)
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        if presenter?.getMovieCount() == 0 {
            tableView.configureEmptyData("No genres available")
        } else {
            tableView.restore()
        }
        return presenter?.getMovieCount() ?? 0
    }

    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: movieCellID,
                                                       for: indexPath) as? MovieCell else {
            return UITableViewCell()
        }
        if let movie = presenter?.getMovieAt(index: indexPath.row) {
            cell.configureCell(with: movie)
        }
        return cell
    }

    override func tableView(_ tableView: UITableView,
                            didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        guard let selectedMovie = presenter?.getMovieAt(index: indexPath.row) else { return }
        let movieDetailVC = MovieListRouter.createMovieDetailModule()
        movieDetailVC.movie = selectedMovie
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(movieDetailVC,
                                                          animated: true)
        }
    }

    // MARK: - Table View Delegate
    override func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if offsetY > contentHeight - scrollView.frame.height {
            if let isLoading = presenter?.isLoadingMore() {
                if !isLoading {
                    presenter?.fetchMoreMovies(with: genre?.id ?? 0)
                }
            }
        }
    }
}

// MARK: - Presenter to View
extension MovieListViewController: MovieListPresenterToViewProtocol {
    func showMovies() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

    func showError() {
        DispatchQueue.main.async {
            AlertView.showErrorFetching(view: self,
                                        title: Constants.generalErrorMessage,
                                        message: Constants.errorFetchingMoviesMSg)
        }
    }
}
