//
//  APIClient.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 06/02/24.
//

import Foundation

struct APIClient {
    /// Shared singleton instance
    static let `default` = APIClient.self

    /// Handlers
    typealias GenreHandler = (Result<Genres, Error>) -> Void
    typealias MoviesHandler = (Result<Movies, Error>) -> Void
    typealias MovieHandler = (Result<Movie, Error>) -> Void
    typealias ReviewHandler = (Result<Reviews, Error>) -> Void
    typealias VideosHandler = (Result<Videos, Error>) -> Void

    static func fetchGenres(then handler: @escaping GenreHandler) {
        /// EndPoint
        let endPoint = APIEndPoint.getGenres()

        /// Composing Url components
        guard let url = endPoint.url else {
            return handler(.failure(.brokenURL))
        }

        /// URL Request configuration
        let urlSession = URLSession.shared
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = HTTPMethod.get.rawValue
        urlRequest.allHTTPHeaderFields = generateHeader()

        /// Perform network call
        urlSession.dataTask(with: urlRequest) { result in
            switch result {
            case .success(let data):
                do {
                    let decoder = JSONDecoder()
                    let data = try decoder.decode(Genres.self, from: data)
                    handler(.success(data))
                } catch let error {
                    handler(.failure(.serialization(String(describing: error))))
                }
            case .failure(let error):
                handler(.failure(.http(String(describing: error))))
            }
        }.resume()
    }

    static func fetchMovieByGenre(with genreId: Int,
                                  page: Int,
                                  then handler: @escaping MoviesHandler) {
        /// EndPoint
        let endPoint = APIEndPoint.getMoviesByGenre(with: genreId,
                                                    page: page)

        /// Composing Url components
        guard let url = endPoint.url else {
            return handler(.failure(.brokenURL))
        }

        /// URL Request configuration
        let urlSession = URLSession.shared
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = HTTPMethod.get.rawValue
        urlRequest.allHTTPHeaderFields = generateHeader()

        /// Perform network call
        urlSession.dataTask(with: urlRequest) { result in
            switch result {
            case .success(let data):
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let data = try decoder.decode(Movies.self, from: data)
                    handler(.success(data))
                } catch let error {
                    handler(.failure(.serialization(String(describing: error))))
                }
            case .failure(let error):
                handler(.failure(.http(String(describing: error))))
            }
        }.resume()
    }

    static func fetchMovieDetail(with movieID: Int,
                                 then handler: @escaping MovieHandler) {
        /// EndPoint
        let endPoint = APIEndPoint.getMovieDetail(with: movieID)

        /// Composing Url components
        guard let url = endPoint.url else {
            return handler(.failure(.brokenURL))
        }

        /// URL Request configuration
        let urlSession = URLSession.shared
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = HTTPMethod.get.rawValue
        urlRequest.allHTTPHeaderFields = generateHeader()

        /// Perform network call
        urlSession.dataTask(with: urlRequest) { result in
            switch result {
            case .success(let data):
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let data = try decoder.decode(Movie.self, from: data)
                    handler(.success(data))
                } catch let error {
                    handler(.failure(.serialization(String(describing: error))))
                }
            case .failure(let error):
                handler(.failure(.http(String(describing: error))))
            }
        }.resume()
    }

    static func fetchMovieReview(with movieID: Int,
                                 page: Int,
                                 then handler: @escaping ReviewHandler) {
        /// EndPoint
        let endPoint = APIEndPoint.getReviews(with: movieID,
                                              page: page)

        /// Composing Url components
        guard let url = endPoint.url else {
            return handler(.failure(.brokenURL))
        }

        /// URL Request configuration
        let urlSession = URLSession.shared
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = HTTPMethod.get.rawValue
        urlRequest.allHTTPHeaderFields = generateHeader()

        /// Perform network call
        urlSession.dataTask(with: urlRequest) { result in
            switch result {
            case .success(let data):
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let data = try decoder.decode(Reviews.self, from: data)
                    handler(.success(data))
                } catch let error {
                    handler(.failure(.serialization(String(describing: error))))
                }
            case .failure(let error):
                handler(.failure(.http(String(describing: error))))
            }
        }.resume()
    }

    static func fetchRelatedVideos(with movieID: Int, 
                                   then handler: @escaping VideosHandler) {
        /// EndPoint
        let endPoint = APIEndPoint.getRelatedVideos(with: movieID)

        /// Composing Url components
        guard let url = endPoint.url else {
            return handler(.failure(.brokenURL))
        }

        /// URL Request configuration
        let urlSession = URLSession.shared
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = HTTPMethod.get.rawValue
        urlRequest.allHTTPHeaderFields = generateHeader()

        /// Perform network call
        urlSession.dataTask(with: urlRequest) { result in
            switch result {
            case .success(let data):
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let data = try decoder.decode(Videos.self, from: data)
                    handler(.success(data))
                } catch let error {
                    handler(.failure(.serialization(String(describing: error))))
                }
            case .failure(let error):
                handler(.failure(.http(String(describing: error))))
            }
        }.resume()
    }

}

// MARK: - Helpers
private extension APIClient {
    static func generateHeader() -> [String : String] {
        let headers = [
          "Accept": "application/json",
          "Content-Type": "application/json"
        ]
        return headers
    }
}

// MARK: - Enums
extension APIClient {
    enum Error: Swift.Error, Equatable {
        case brokenURL
        case serialization(String)
        case http(String)
    }

    enum Result<Value, Error: Swift.Error> {
        case success(Value)
        case failure(Error)
    }

    enum HTTPMethod: String {
        case get, post, put, delete, patch, head
    }
}
