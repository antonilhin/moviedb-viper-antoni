//
//  APIEndPoint.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 06/02/24.
//

import Foundation

struct APIEndPoint {
    let path: String
    let queryItems: [URLQueryItem]?
}

// MARK: - Constructing URLComponents
extension APIEndPoint {
    var url: URL? {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "api.themoviedb.org"
        components.path = path
        components.queryItems = queryItems
        return components.url
    }
}

// MARK: - End Points
extension APIEndPoint {
    static func getGenres() -> APIEndPoint {
        let mandatoryQuery = generateMandatoryQueryItems()
        return APIEndPoint(path: "/3/genre/movie/list",
                           queryItems: mandatoryQuery)
    }

    static func getMoviesByGenre(with genreID: Int,
                                 page: Int) -> APIEndPoint {
        let path = "/3/discover/movie"

        var queryItems = generateMandatoryQueryItems()
        let genreId = URLQueryItem(name: "with_genres", value: String(genreID))
        let page = URLQueryItem(name: "page", value: String(page))
        queryItems.append(genreId)
        queryItems.append(page)

        return APIEndPoint(path: path,
                           queryItems: queryItems)
    }

    static func getMovieDetail(with movieID: Int) -> APIEndPoint {
        let path = "/3/movie/\(movieID)"
        let mandatoryQuery = generateMandatoryQueryItems()
        return APIEndPoint(path: path,
                           queryItems: mandatoryQuery)
    }

    static func getReviews(with movieID: Int,
                           page: Int) -> APIEndPoint {
        let path = "/3/movie/\(movieID)/reviews"
        var queryItems = generateMandatoryQueryItems()
        let page = URLQueryItem(name: "page", value: String(page))
        queryItems.append(page)
        return APIEndPoint(path: path,
                           queryItems: queryItems)
    }

    static func getRelatedVideos(with movieID: Int) -> APIEndPoint {
        let path = "/3/movie/\(movieID)/videos"
        let mandatoryQuery = generateMandatoryQueryItems()
        return APIEndPoint(path: path,
                           queryItems: mandatoryQuery)
    }
}

// MARK: - Private helpers
extension APIEndPoint {
    static func generateMandatoryQueryItems() -> [URLQueryItem] {
        var queryItems = [URLQueryItem]()
        let languangeQuery = URLQueryItem(name: "language", value: "en-US")
        let apiKey = URLQueryItem(name: "api_key", value: "34b106da35243b6a9bd8264787d00694")

        queryItems.append(apiKey)
        queryItems.append(languangeQuery)

        return queryItems
    }
}
