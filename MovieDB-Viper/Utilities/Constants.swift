//
//  Constants.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 07/02/24.
//

import Foundation

struct Constants {
    static let imageBaseUrl = "https://image.tmdb.org/t/p/original"
    static let youTubeWatchBaseUrl = "https://www.youtube.com/watch?v="

    static let generalErrorMessage = "Please try again later."
    static let errorFetchingMoviesMSg = "Problem fetching movies"
    static let errorFetchingGenresMsg = "Problem fetching genres"
    static let errorFetchingReviewsMsg = "probelm fetching reviews"
    static let errorFetchingMovieDetail = "Problem fetching movie detail"
}
