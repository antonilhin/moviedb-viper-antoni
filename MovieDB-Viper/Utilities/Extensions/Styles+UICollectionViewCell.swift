//
//  Styles+UICollectionViewCell.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 07/02/24.
//

import Foundation
import UIKit

extension UICollectionViewCell {
    func addBorderAndShadow() {
        // Border
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 10

        // Shadow
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 2.0, height: 4.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
    }
}
