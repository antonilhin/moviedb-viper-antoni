//
//  URLSession+DataTask.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 06/02/24.
//

import Foundation

extension URLSession {
    func dataTask(
        with url: URLRequest,
        handler: @escaping (Result<Data, Error>) -> Void) -> URLSessionDataTask {
            dataTask(with: url) { data, _, error in
                if let error = error {
                    handler(.failure(error))
                } else {
                    handler(.success(data ?? Data()))
                }
            }
        }
}
