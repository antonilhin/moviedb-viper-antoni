//
//  ReuseableAlertView.swift
//  MovieDB-Viper
//
//  Created by Antoni Lin on 08/02/24.
//

import Foundation
import UIKit

class AlertView: NSObject {
    static func showErrorFetching(view: UIViewController ,
                                 title: String ,
                                 message: String){
        let alert = UIAlertController(title: title,
                                      message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok",
                                      style: .default,
                                      handler: nil))
        view.present(alert, animated: true, completion: nil)
    }
}
