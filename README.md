# TMDB-Viper

## Features

 - [x] List of official genres for movies.
 - [x] List of discover movies by genre.
 - [x] Endless scrolling for movies and reviews list.

## Approaches
- Interface: UIKit & Swift.
- Design pattern: Viper.
- Result type for Networking layer.
- Third party - SPM - [`SDWebImage`](https://github.com/SDWebImage/SDWebImage) - Handle async image downloader with cache support.
